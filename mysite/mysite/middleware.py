from primes.models import Numberss
from django.utils.deprecation import MiddlewareMixin

class PrimesMiddleware(MiddlewareMixin):
    def _init_(self, get_response):
        self.get_response = get_response

    def _call_(self, request):

        # Code that is executed in each request before the view is called

        response = self.get_response(request)

        # Code that is executed in each request after the view is called
        return response

    def process_request(self, request):

        if request.method == 'POST':
            nums = Numberss(total_primes = request.POST['num1'])
            nums.save()

