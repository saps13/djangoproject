from __future__ import absolute_import, unicode_literals
from celery import task
from primes.models import Numberss
import math

@task()
def find_primes():
    count = 1
    j = 2
    res = ''
    var = Numberss.objects.latest("total_primes")
    while count < var.total_primes:

        flag = True
        for i in range(2, int(math.sqrt(j))+1):
            if j % i == 0 :
                flag = False

        if flag:
            res = res + str(j) + ' '
            count+=1

        j+=1

    print(res)


