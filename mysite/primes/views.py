# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from primes.models import Numberss
from primes.tasks import find_primes
import math

# Create your views here.

def index(request):
    return render(request, "input.html")

def primenumbers(request):

    #Numberss.objects.create(total_primes = request.POST['num1'])
    find_primes.delay()
    return render(request, "result.html", {"requestid": 3, "status": "processing"})
